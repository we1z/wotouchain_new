/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    config.toolbarGroups = [
        {name: 'document', groups: ['mode', 'document', 'doctools']},
        {name: 'clipboard', groups: ['clipboard', 'undo']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing']},
        {name: 'forms', groups: ['forms']},
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph']},
        {name: 'styles', groups: ['styles']},
        {name: 'colors', groups: ['colors']},
        {name: 'insert', groups: ['insert']},
        '/',
        {name: 'links', groups: ['links']},
        '/',
        {name: 'tools', groups: ['tools']},
        {name: 'others', groups: ['others']},
        {name: 'about', groups: ['about']}
    ];

    config.removeButtons = 'Source,Save,NewPage,Preview,Print,Scayt,SelectAll,Find,Replace,Cut,Templates,Paste,PasteText,PasteFromWord,Copy,EasyImageUpload,Flash,HorizontalRule,Smiley,Iframe,PageBreak,Maximize,ShowBlocks,About,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Anchor,Unlink,Link,BidiLtr,BidiRtl,Language,Blockquote,CreateDiv';

    config.extraPlugins = 'imagepaste';

    config.filebrowserImageUploadUrl = '/upload/';

    config.extraPlugins = 'uploadimage';

    config.imageUploadUrl = '/upload/';

    config.resize_enabled = false;

    config.height = 300;

    config.uiColor = '#FFFFFF';

    config.language = 'zh-cn';

    config.pasteFromWordIgnoreFontFace = true;

    config.undoStackSize = 20;

    config.forcePasteAsPlainText = true;

};