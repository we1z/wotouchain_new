$(function () {
    reset_pd();
});

function reset_pd() {
    $("#hand_in").click(function () {
        var email = $('#email').val();
        var password = $('#password').val();
        var repeat_password = $('#repeat_password').val();
        var username = document.getElementById("uid").value;
        var verify_code = document.getElementById("spvc").value;
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/reset_pd/",
            type: 'POST',
            data: {email: email, password: password, repeat_password: repeat_password,username:username,verify_code:verify_code},
            dataType: 'json',
            success: function (arg) {
                if (arg.type == 1) {
                    $("#hand_in_error").empty();
                    $("#hand_in_error").append("<p class='text-danger' style='margin: 0;padding: 0'>" + arg.msg + "</p>");
                }
                else {
                    alert(arg.msg);
                    window.location.href = "/log_in/"
                }
            }
        });
    });
}