$(function () {
    log_in();
});

function log_in() {
    $("#log_in").click(function () {
        var email = $('#email').val();
        var password = $('#password').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/log_in/",
            type: 'POST',
            data: {email: email, password: password},
            dataType: 'json',
            success: function (arg) {
                if (arg.type == 1) {
                    $("#log_in_error").empty();
                    $("#log_in_error").append("<p class='text-danger' style='margin: 0;padding: 0'>" + arg.msg + "</p>");
                }
                else {
                    window.location.href="/home/"
                }
            }
        });
    });
}