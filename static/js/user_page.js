$(function column() {
    $('#sidebar li').click(function () {
        $("#sidebar li").removeClass('active');
        $(this).addClass('active');
        var title = this.innerText;
        document.getElementById("main-header").innerHTML = title;
        $("#main").empty();
    });
});

function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");

    if (arr = document.cookie.match(reg))

        return unescape(arr[2]);
    else
        return null;
}

$(document).ready(function () {
    $("#l-0").click(function () {
        $("#main").load("u-main.html",);
    });
    $("#l-1").click(function () {
        $("#main").load("u-profile.html", function () {
            $("#nickname-save").click(function () {
                var nick_name = $('#nickname').val();
                $.ajaxSetup({
                    data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
                });
                $.ajax({
                    url: "/u-profile/",
                    type: 'POST',
                    data: {nick_name: nick_name},
                    dataType: 'json',
                    success: function (arg) {
                        if (arg.msg === 1) {
                            $('#n-close').click();
                            $("#nick_name").empty();
                            $("#nick_name").append(nick_name);
                        }
                        else {
                            alert(arg.msg)
                        }
                    }
                });
            });
            $("#tel_num-save").click(function () {
                var tel_num = $('#tel_num').val();
                $.ajaxSetup({
                    data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
                });
                $.ajax({
                    url: "/u-profile/",
                    type: 'POST',
                    data: {tel_num: tel_num},
                    dataType: 'json',
                    success: function (arg) {
                        if (arg.msg === 1) {
                            $('#t-close').click();
                            $("#handy").empty();
                            $("#handy").append(tel_num);
                        }
                        else {
                            alert(arg.msg)
                        }
                    }
                });
            });
            $("#email_bak-save").click(function () {
                var email_bak = $('#email_bak').val();
                $.ajaxSetup({
                    data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
                });
                $.ajax({
                    url: "/u-profile/",
                    type: 'POST',
                    data: {email_bak: email_bak},
                    dataType: 'json',
                    success: function (arg) {
                        if (arg.msg === 1) {
                            $('#e-close').click();
                            $("#emailbak").empty();
                            $("#emailbak").append(email_bak);
                        }
                        else {
                            alert(arg.msg)
                        }
                    }
                });
            });
            $("#identification_photo-save").click(function () {
                var formData = new FormData();
                formData.append("audit_a", $("#audit_a")[0].files[0]);
                formData.append("audit_b", $("#audit_b")[0].files[0]);
                $.ajaxSetup({
                    headers: {"X-CSRFToken": getCookie("csrftoken")}
                });
                $.ajax({
                    url: '/identification_photo/',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function (arg) {
                        if (arg.msg === 1) {
                            $('#p-close').click();
                            $("#audit_state").empty();
                            $("#audit_state").append('审核中');
                        }
                        else {
                            alert(arg.msg)
                        }
                    }
                });
            });
        });
    });
});
