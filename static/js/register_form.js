$(function () {
    check_email();
    check_password();
    check_nick_name();
    check_real_name();
    check_id_num();
    check_tel_num();
    check_repeat_password();
});

function check_email() {
    $("#email").blur(function () {
        var email = $('#email').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/register/",
            type: 'POST',
            data: {email: email, check_email: 1},
            dataType: 'json',
            success: function (arg) {
                if (arg.msg.length < 20) {
                    if (arg.type == 1) {
                        $("#email_error").empty();
                        $("#email_error").append("<p class='text-danger' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");
                    }
                    else {
                        $("#email_error").empty();
                        $("#email_error").append("<p class='text-success' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");

                    }
                }
            }
        });
    });
}

function check_password() {
    $("#password").blur(function () {
        var password = $('#password').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/register/",
            type: 'POST',
            data: {password: password, check_password: 1},
            dataType: 'json',
            success: function (arg) {
                if (arg.msg.length < 20) {
                    if (arg.type == 1) {
                        $("#password_error").empty();
                        $("#password_error").append("<p class='text-danger' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");
                    }
                    else {
                        $("#password_error").empty();
                        $("#password_error").append("<p class='text-success' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");

                    }
                }
            }
        });
    });
}

function check_repeat_password() {
    $("#repeat_password").blur(function () {
        var repeat_password = $('#repeat_password').val();
        var password = $('#password').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/register/",
            type: 'POST',
            data: {repeat_password: repeat_password,password:password, check_repeat_password: 1},
            dataType: 'json',
            success: function (arg) {
                if (arg.msg.length < 20) {
                    if (arg.type == 1) {
                        $("#repeat_password_error").empty();
                        $("#repeat_password_error").append("<p class='text-danger' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");
                    }
                    else {
                        $("#repeat_password_error").empty();
                        $("#repeat_password_error").append("<p class='text-success' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");

                    }
                }
            }
        });
    });
}

function check_nick_name() {
    $("#nick_name").blur(function () {
        var nick_name = $('#nick_name').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/register/",
            type: 'POST',
            data: {nick_name: nick_name, check_nick_name: 1},
            dataType: 'json',
            success: function (arg) {
                if (arg.msg.length < 20) {
                    if (arg.type == 1) {
                        $("#nick_name_error").empty();
                        $("#nick_name_error").append("<p class='text-danger' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");
                    }
                    else {
                        $("#nick_name_error").empty();
                        $("#nick_name_error").append("<p class='text-success' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");

                    }
                }
            }
        });
    });
}

function check_real_name() {
    $("#real_name").blur(function () {
        var real_name = $('#real_name').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/register/",
            type: 'POST',
            data: {real_name: real_name, check_real_name: 1},
            dataType: 'json',
            success: function (arg) {
                if (arg.msg.length < 20) {
                    if (arg.type == 1) {
                        $("#real_name_error").empty();
                        $("#real_name_error").append("<p class='text-danger' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");
                    }
                    else {
                        $("#real_name_error").empty();
                        $("#real_name_error").append("<p class='text-success' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");

                    }
                }
            }
        });
    });
}

function check_id_num() {
    $("#id_num").blur(function () {
        var id_num = $('#id_num').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/register/",
            type: 'POST',
            data: {id_num: id_num, check_id_num: 1},
            dataType: 'json',
            success: function (arg) {
                if (arg.msg.length < 20) {
                    if (arg.type == 1) {
                        $("#id_num_error").empty();
                        $("#id_num_error").append("<p class='text-danger' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");
                    }
                    else {
                        $("#id_num_error").empty();
                        $("#id_num_error").append("<p class='text-success' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");

                    }
                }
            }
        });
    });
}

function check_tel_num() {
    $("#tel_num").blur(function () {
        var tel_num = $('#tel_num').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/register/",
            type: 'POST',
            data: {tel_num: tel_num, check_tel_num: 1},
            dataType: 'json',
            success: function (arg) {
                if (arg.msg.length < 20) {
                    if (arg.type == 1) {
                        $("#tel_num_error").empty();
                        $("#tel_num_error").append("<p class='text-danger' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");
                    }
                    else {
                        $("#tel_num_error").empty();
                        $("#tel_num_error").append("<p class='text-success' style='margin-left: 420px;margin-top: 7px'>" + arg.msg + "</p>");

                    }
                }
            }
        });
    });
}