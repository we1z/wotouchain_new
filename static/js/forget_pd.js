$(function () {
    forget_pd();
});

function forget_pd() {
    $("#hand_in").click(function () {
        var email = $('#email').val();
        var real_name = $('#real_name').val();
        var id_num = $('#id_num').val();
        var tel_num = $('#tel_num').val();
        $.ajaxSetup({
            data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
        });
        $.ajax({
            url: "/forget_pd/",
            type: 'POST',
            data: {email: email, real_name: real_name,id_num:id_num,tel_num:tel_num},
            dataType: 'json',
            success: function (arg) {
                if (arg.type == 1) {
                    $("#hand_in_error").empty();
                    $("#hand_in_error").append("<p class='text-danger' style='margin: 0;padding: 0'>" + arg.msg + "</p>");
                }
                else {
                    alert(arg.msg);
                    window.location.href="/home/"
                }
            }
        });
    });
}