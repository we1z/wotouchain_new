$(function pager() {
    $("#n-next").click(function () {
        if (document.getElementById('page_1').style.display === '') {
            document.getElementById('page_1').setAttribute("style", "display:none;");
            document.getElementById('page_2').setAttribute("style", "display:'';");
            document.getElementById('l-1').setAttribute("style", "color:#1c5e1c;");
        }
        else if (document.getElementById('page_2').style.display === '') {
            document.getElementById('page_2').setAttribute("style", "display:none;");
            document.getElementById('page_3').setAttribute("style", "display:'';");
            document.getElementById('l-2').setAttribute("style", "color:#1c5e1c;");
            document.getElementById('normal_pager').setAttribute("style", "display:none;");
            document.getElementById('submit_pager').setAttribute("style", "display:'';")
        }
    });
    $("#n-last").click(function () {
        if (document.getElementById('page_2').style.display === '') {
            document.getElementById('page_2').setAttribute("style", "display:none;");
            document.getElementById('page_1').setAttribute("style", "display:'';");
            document.getElementById('l-1').setAttribute("style", "color:#8c8c8c;");
        }
    });
    $("#s-last").click(function () {
        if (document.getElementById('page_3').style.display === '') {
            document.getElementById('page_3').setAttribute("style", "display:none;");
            document.getElementById('page_2').setAttribute("style", "display:'';");
            document.getElementById('l-2').setAttribute("style", "color:#8c8c8c;");
            document.getElementById('normal_pager').setAttribute("style", "display:'';");
            document.getElementById('submit_pager').setAttribute("style", "display:none;")
        }
    });
});

$(function add_period() {
    $("#add_period").click(function () {
        if (document.getElementById('p_2').style.display === 'none') {
            document.getElementById('p_2').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_3').style.display === 'none') {
            document.getElementById('p_3').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_4').style.display === 'none') {
            document.getElementById('p_4').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_5').style.display === 'none') {
            document.getElementById('p_5').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_6').style.display === 'none') {
            document.getElementById('p_6').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_7').style.display === 'none') {
            document.getElementById('p_7').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_8').style.display === 'none') {
            document.getElementById('p_8').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_9').style.display === 'none') {
            document.getElementById('p_9').setAttribute("style", "display:'';");
        }
        else if (document.getElementById('p_10').style.display === 'none') {
            document.getElementById('p_10').setAttribute("style", "display:'';");
        }
        else {
            alert('目前一个项目暂时只支持最多10个阶段。')
        }
    })

});

function hand_in() {
    var msg = "是否确定提交项目？";
    return confirm(msg)

}