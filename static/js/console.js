$(function column() {
    $('#sidebar li').click(function () {
        $("#sidebar li").removeClass('active');
        $(this).addClass('active');
        var title = this.innerText;
        document.getElementById("main-header").innerHTML = title;
        $("#main").empty();
    });
});

function getCookie(name) {
    var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");

    if (arr = document.cookie.match(reg))

        return unescape(arr[2]);
    else
        return null;
}


$(document).ready(function () {
    $("#l-0").click(function () {
        $("#main").load("c-main.html",);
    });
    $("#l-1").click(function () {
        $("#main").load("c-nodes.html", function () {
            $("#submit_node").click(function () {
                var node = $('#register_node').val();
                $.ajaxSetup({
                    data: {'csrfmiddlewaretoken': $("[name='csrfmiddlewaretoken']").val()}
                });
                $.ajax({
                    url: "/c_contain/",
                    type: 'POST',
                    data: {event: "register_node", event_value: node},
                    dataType: 'json',
                    success: function (arg) {
                        alert(arg.msg);
                        $("#main").load("c-nodes.html");
                    }
                });
            });
        });
    });
    $("#l-2").click(function () {
        $("#main").load("c-p-audit.html", function () {
            $("[id^=check-]").click(function () {
                var pid = $(this).attr("id").split("check-")[1];
                $.ajaxSetup({
                    headers: {"X-CSRFToken": getCookie("csrftoken")}
                });
                $("#main").load("c-p-detail.html", {pid: pid}, function () {

                })
            });
        });
    });
    $("#l-3").click(function () {
        $("#main").load("c-u-audit.html", function () {
            $("[id^=yes-]").click(function () {
                var id = $(this).attr("id").split("yes-")[1];
                $.ajaxSetup({
                    headers: {"X-CSRFToken": getCookie("csrftoken")}
                });
                $.ajax({
                    url: "/c-u-audit/",
                    type: 'POST',
                    data: {state: 1, id: id},
                    dataType: 'json',
                    success: function (arg) {
                        $("#" + arg.c).click();
                        $("#" + arg.a).empty();
                        $("#" + arg.a).append('通过审核');
                    }
                });
            });
            $("[id^=no-]").click(function () {
                var id = $(this).attr("id").split("no-")[1];
                $.ajaxSetup({
                    headers: {"X-CSRFToken": getCookie("csrftoken")}
                });
                $.ajax({
                    url: "/c-u-audit/",
                    type: 'POST',
                    data: {state: 0, id: id},
                    dataType: 'json',
                    success: function (arg) {
                        $("#" + arg.c).click();
                        $("#" + arg.a).empty();
                        $("#" + arg.a).append('审核失败');
                    }
                });
            });
        });
    });
});
