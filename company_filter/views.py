from django.shortcuts import render
from datetime import datetime
from main.db_connection import mongodb_connection
from .baidu_score import cal_baidu_score_api
from .baidu_text import search_company_baidu_info_api
import json
from django.http import HttpResponseRedirect, HttpResponse
import time
import requests


def get_company_list(register_date_l, register_date_u, money_l, money_u, industry, district, stock, foreign, crm):
    company_list = []
    if industry == '0':
        if district == '0':
            if stock == '2':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False
                             })]
            elif stock == '1':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': False
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': False
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': False
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': False
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': False
                             })]
            elif stock == '0':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': True
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': True
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': True
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': True
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': True
                             })]
        elif district == '1':
            district_list = [i['city'] for i in mongodb_connection('company')['whitelist_district'].find()]
            if stock == '2':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '地区': {'$in': district_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '地区': {'$in': district_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '地区': {'$in': district_list}
                             })]
            elif stock == '1':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': False,
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '地区': {'$in': district_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': False,
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': False,
                             '地区': {'$in': district_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': False,
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': False,
                             '地区': {'$in': district_list}
                             })]
            elif stock == '0':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': True,
                             '地区': {'$in': district_list}

                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '地区': {'$in': district_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': True,
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': True,
                             '地区': {'$in': district_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': True,
                             '地区': {'$in': district_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': True,
                             '地区': {'$in': district_list}
                             })]
    elif industry == '1':
        industry_list = [i['industry'] for i in mongodb_connection('company')['whitelist_industry'].find()]
        if district == '0':
            if stock == '2':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '行业': {'$in': industry_list}
                             })]
            elif stock == '1':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': False,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': False,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': False,
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': False,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': False,
                             '行业': {'$in': industry_list}
                             })]
            elif stock == '0':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': True,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': True,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': True,
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': True,
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': True,
                             '行业': {'$in': industry_list}
                             })]
        elif district == '1':
            district_list = [i['city'] for i in mongodb_connection('company')['whitelist_district'].find()]
            if stock == '2':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
            elif stock == '1':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': False,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': False,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': False,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': False,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': False,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
            elif stock == '0':
                if foreign == '2':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '是否上市': True,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}

                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '1':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': True,
                             '是否上市': True,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': True,
                             '是否上市': True,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                elif foreign == '0':
                    if crm == '0':
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '国外法人': False,
                             '是否上市': True,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
                    else:
                        crm_list = [i['企业全称'] for i in mongodb_connection('company')['trace_company'].find()]
                        company_list = [i for i in mongodb_connection('company')['raw_company'].find(
                            {'成立日期': {'$gte': register_date_l, '$lte': register_date_u},
                             '注册资本': {'$gte': money_l, '$lte': money_u},
                             '企业全称': {'$nin': crm_list},
                             '国外法人': False,
                             '是否上市': True,
                             '地区': {'$in': district_list},
                             '行业': {'$in': industry_list}
                             })]
    return company_list


def company_filter(request):
    if request.method == 'GET':
        return render(request, 'company_filter.html')
    elif request.method == 'POST':
        register_date_l = request.POST.get('register_date_l')
        register_date_u = request.POST.get('register_date_u')
        money_l = request.POST.get('money_l')
        money_u = request.POST.get('money_u')
        industry = request.POST.get('industry')
        district = request.POST.get('district')
        stock = request.POST.get('stock')
        foreign = request.POST.get('foreign')
        crm = request.POST.get('crm')
        register_date_l = datetime.strptime(register_date_l, '%Y-%m-%d').strftime('%Y-%m-%d')
        register_date_u = datetime.strptime(register_date_u, '%Y-%m-%d').strftime('%Y-%m-%d')
        money_l = int(money_l)
        money_u = int(money_u)
        company_list = get_company_list(register_date_l, register_date_u, money_l, money_u, industry, district, stock,
                                        foreign, crm)
        return render(request, 'search_result.html', {'company_list': company_list})


def search_result(request):
    return render(request, 'search_result.html')


def update_ops(request):
    if request.method == 'POST':
        company_info = request.POST.get('company_info')
        company_name = company_info.split('_')[0]
        register_date = company_info.split('_')[1]

        requests.post('http://localhost:9527/nameko_dispatch_keyword', json={"company_name":company_name,"register_date":register_date})

        now=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
        c=0
        while "上次ops分数更新" not in mongodb_connection('company')['raw_company'].find_one({"企业全称": company_name}):
            if c>10:
                break
            else:
                time.sleep(1)
        while now > mongodb_connection('company')['raw_company'].find_one({"企业全称": company_name})['上次ops分数更新']:
            time.sleep(2)
        score=mongodb_connection('company')['raw_company'].find_one({"企业全称": company_name})['ops']
        return HttpResponse(json.dumps({'id': company_name, 'score': score}))
