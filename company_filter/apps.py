from django.apps import AppConfig


class CompanyFilterConfig(AppConfig):
    name = 'company_filter'
