from nameko.events import EventDispatcher, event_handler
import time
from baidu_score import cal_baidu_score_api
from baidu_text import search_company_baidu_info_api
from nameko.web.handlers import http
from db_connection import mongodb_connection
import json
import logging


logging.basicConfig(format='%(asctime)s-----%(message)s')
logger=logging.getLogger('ops_recommand')



class HttpService:
    '''
    http服务端
    '''

    def __init__(self):
        pass

    name = "http_service"

    dispatch = EventDispatcher()

    @http('POST', '/nameko_dispatch_keyword')
    def dispatching_keyword_method(self, request):
        '''
        分发任务
        :param request:
        :return:
        '''
        data_json = json.loads(request.get_data(as_text=True))
        company_name = data_json['company_name']
        register_date = data_json['register_date']
        self.dispatch("update_ops", (company_name, register_date))
        return 200, json.dumps({'state': 'ok'})


class Spider:
    """ Event listening service. """

    def __init__(self):
        pass

    name = "spider"

    @event_handler('http_service', 'update_ops')
    def update_ops(self, args):
        """
        更新ops分数
        :param request:
        :return:
        """

        company_name = args[0]
        register_date = args[1]
        text = search_company_baidu_info_api(company_name)
        score = cal_baidu_score_api(text, register_date)
        mongodb_connection('company')['company_intro'].update({"企业全称": company_name},
                                                              {"$set": {"百度信息": text, "ops": True}})
        mongodb_connection('company')['raw_company'].update({"企业全称": company_name}, {
            "$set": {"ops": score, "上次ops分数更新": time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))}})

        logger.info("公司名：{},最后得分：{}".format(company_name,score))
