"""wotouchain URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.contrib import admin
from main import views
from django.views.static import serve
from wotouchain import settings
import company_filter.views



urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'test', views.test, name='test'),
    url(r'^home', views.home,name='home'),
    url(r'^register$', views.register,name='register'),
    url(r'^register_verify', views.register_verify, name='register_verify'),
    url(r'^send_reset_pd_verify_code', views.send_reset_pd_verify_code, name='send_reset_pd_verify_code'),
    url(r'^log_in', views.log_in, name='log_in'),
    url(r'^forget_pd', views.forget_pd, name='forget_pd'),
    url(r'^reset_pd', views.reset_pd, name='reset_pd'),
    url(r'^start_crowdfunding', views.start_crowdfunding, name='start_crowdfunding'),
    url(r'^upload/$', views.upload_file, name='upload_file'),
    url(r'^upload/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^administrator', views.administrator, name='administrator'),
    url(r'^console', views.console, name='console'),
    url(r'^c_contain', views.console_contain, name='c_contain'),
    url(r'^c-main', views.console_main, name='c_main'),
    url(r'^c-nodes', views.console_nodes, name='c_nodes'),
    url(r'^c-p-audit', views.console_audit, name='c_p_audit'),
    url(r'^c-u-audit', views.console_user_audit, name='c_u_audit'),
    url(r'^user-page', views.user_page, name='user_page'),
    url(r'^logout', views.logout, name='logout'),
    url(r'^u-main', views.user_main, name='user_main'),
    url(r'^u-profile', views.user_profile, name='user_profile'),
    url(r'^project_detail', views.project_detail, name='project_detail'),
    url(r'^identification_photo', views.identification_photo, name='identification_photo'),
    url(r'^c-p-detail', views.project_audit, name='project_audit'),

    url(r'^company_filter', company_filter.views.company_filter, name='company_filter'),
    url(r'^search_result', company_filter.views.search_result, name='search_result'),
    url(r'^update_ops', company_filter.views.update_ops, name='update_ops'),
]
