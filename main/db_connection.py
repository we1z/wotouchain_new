# -*- coding:utf-8-*-#

"""
管理数据库链接
"""

import pymongo
import time

# Mongodb配置
MONGO_SERVER = '127.0.0.1'
MONGO_PORT = 27017

# 数据库账户密码
COMPANY_DB_USER = 'UserCompany'
COMPANY_DB_PW = 'ihcg27epb5'

CORPUS_DB_USER = 'UserCorpus'
CORPUS_DB_PW = '0onh2wsgzc'

NEWS_DB_USER = ''
NEWS_DB_PW = ''

SERVER_DB_USER = 'UserServer'
SERVER_DB_PW = '2rfhu7zx5k'

company_db = None
corpus_db = None
news_db = None
server_db=None


def mongodb_connection(db, is_reused=False):
    """
    获取数据库链接
    :param db: 数据库名称
    :param is_reused 是否复用数据库链接，若链接长期不用，则False，若短时间内（1分钟以内）则选True
    :return: 数据库链接
    """
    global company_db
    global corpus_db
    global news_db
    global server_db

    is_reused=False

    if db == 'company':
        if company_db and is_reused:
            return company_db
        else:
            user = COMPANY_DB_USER
            pw = COMPANY_DB_PW
    elif db == 'corpus':
        if corpus_db and is_reused:
            return corpus_db
        else:
            user = CORPUS_DB_USER
            pw = CORPUS_DB_PW
    elif db == 'news':
        if news_db and is_reused:
            return news_db
        else:
            user = NEWS_DB_USER
            pw = NEWS_DB_PW
    elif db == 'server':
        if server_db and is_reused:
            return server_db
        else:
            user = SERVER_DB_USER
            pw = SERVER_DB_PW

    conn = pymongo.MongoClient(MONGO_SERVER, MONGO_PORT)

    try:
        conn.database.authenticate(user, pw, db)
    except pymongo.errors.NetworkTimeout:
        time.sleep(1)
        return mongodb_connection(db, is_reused)
    except pymongo.errors.ServerSelectionTimeoutError:
        time.sleep(1)
        return mongodb_connection(db, is_reused)
    if db == 'company':
        company_db = conn[db]
        return company_db
    elif db == 'corpus':
        corpus_db = conn[db]
        return corpus_db
    elif db == 'news':
        news_db = conn[db]
        return news_db
    elif db == 'server':
        server_db = conn[db]
        return server_db

