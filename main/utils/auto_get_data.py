from wotouchain.main import utils


def count_project_amount(project, goal=10000):
    """
    :param project:项目ID
    :param goal:总目标
    :return: 项目名称筹资总额
    """
    count = 0
    chains = utils.full_chain()
    for block in chains['chain']:
        if project in block['companyID']:
            if 'transactions' in block.keys():
                for tran in block['transactions']:
                    if 'companyID' in tran.keys() and 'status' in tran.keys():
                        if tran['companyID'] == project and tran['status'] == 'promise':
                            count += int(tran['amount'])
    progress = str(round(count/goal, 3)*100)+'%'
    return count, progress


def count_somebody_amount(sender, cid, stage=None):
    """
    :param sender: 投资人
    :param cid: 投资项目ID
    :param stage: 批次
    :return: 投资人在某批次上，此项目上允诺总额和实际付款总额
    """
    chains = utils.full_chain()
    count = 0
    true_tran = 0
    for block in chains['chain']:
        if cid in block['companyID']:
            if 'transactions' in block.keys():
                for tran in block['transactions']:
                    if tran['sender'] == sender and tran['companyID'] == cid and tran['status'] == 'promise':
                        if stage:
                            if tran['stage'] == stage:
                                count += int(tran['amount'])
                        else:
                            count += int(tran['amount'])
                    elif tran['sender'] == sender and tran['companyID'] == cid and tran['status'] == 'true_trans':
                        if stage:
                            if tran['stage'] == stage:
                                true_tran += int(tran['amount'])
                        else:
                            true_tran += int(tran['amount'])
    return count, true_tran


def count_somebody_mining_time(sender):
    chains = {
        'chain': utils.blockchain.chain,
        'length': len(utils.blockchain.chain),
    }
    count = 0
    for block in chains['chain']:
        if 'transactions' in block.keys():
            for tran in block['transactions']:
                if tran['sender'] == sender and 'mining_time' in tran.keys():
                    count += int(tran['mining_time'])
    return count


def find_prove_dic(pro_id):
    """
    :param pro_id:
    :return: 资料字典 {'文件名'：'数据'}
    """
    chain = utils.full_chain()
    for block in chain['chain']:
        if int(pro_id) in block['companyID'] and 'company_prove' in block.keys():
            return block['company_prove'][0]['company_prove']      # 没有考虑资料更新


def find_user_project(email, types):
    """
    :return: 返回用户所有交易的项目ID和对应的金额,和
             {'companyID':{'sum':sum,'stage_amount': {'stage':amount...}},...}
    """
    project_dict = {}
    chain = utils.full_chain()
    for block in chain['chain']:
        if 'transactions' in block.keys():
            for trans in block['transactions']:
                if trans['sender'] == email and trans['status'] == types:
                    if trans['companyID'] in project_dict.keys():
                        project_dict[trans['companyID']]['sum'] += int(trans['amount'])
                        if trans['stage'] in project_dict[trans['companyID']]['stage_amount'].keys():
                            project_dict[trans['companyID']]['stage_amount'][trans['stage']] += int(trans['amount'])
                        else:
                            project_dict[trans['companyID']]['stage_amount'][trans['stage']] = int(trans['amount'])
                    else:
                        project_dict[trans['companyID']] = {}
                        project_dict[trans['companyID']]['sum'] = int(trans['amount'])
                        project_dict[trans['companyID']]['stage_amount'] = {}
                        project_dict[trans['companyID']]['stage_amount'][trans['stage']] = int(trans['amount'])

    return project_dict
