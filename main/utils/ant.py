import random
from .ping import Ping


# 该类是模仿蚂蚁寻路算法，但暂时只模仿了嗅探部分，结合后续部分基本够用。
# 如果以后出现了非常大的高并发情况以及上万数量级的节点，修改这个类为完整版本的蚂蚁寻路算法即可
# 因为目前所有节点都只可能会在中国，所以嗅探部分没有加入判断ip距离部分。
# 如果日后打开国际市场，还请自行在ping类加入具体的判断方法
class ant:
    def __init__(self):
        # 记载单只蚂蚁所访问过的节点
        self.nodes = []
        # 记录单只蚂蚁在访问完全部节点后的最短路径
        self.min_ant_nodes = []
        # 记录节点上信息素的多少
        self.alive_node = []

    def ping(self, nodes):
        # 向服务器发起ping请求
        ping = Ping()
        ping.get_ips(nodes)
        alive_ip = ping.run_threads()
        self.alive_node = alive_ip

    def get_nodes(self, nodes):
        ant_nodes = nodes
        # 设置种子，万一程序出错可借由此进行debug
        # random.seed(10)
        # 具体每次运行该算法需要随机提取几个节点，因为我这边没有现成的数据实验，到时候还请自行实验scikit库中网格搜索匹配
        ant_node = random.sample(ant_nodes, 10)

        # 向服务器发起ping请求，可通信的节点将会被列入alive_node列表之中
        alive_nodes = []
        for node in ant_node:
           alive_node = self.ping(node)
           alive_nodes.append(alive_node)
           if sum(alive_nodes) >8:
               return alive_nodes
