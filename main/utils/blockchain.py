# coding:utf-8
import hashlib
import pickle
from time import time
from typing import Any, Dict, List, Optional
from urllib.parse import urlparse
import os
from collections import OrderedDict
from wotouchain.wotouchain.settings import BASE_DIR
import requests

from wotouchain.main.utils.auto_get_data import count_somebody_mining_time
import threading
import re
from wotouchain.main.db_connection import mongodb_connection
import socket


class Blockchain:
    def __init__(self):
        self.current_transactions = []  # 交易信息
        self.current_introduction = []  #
        self.chain = []
        self.nodes = set()
        self.companyID = []
        self.current_prove = []
        self.funder_prove = []
        self.company_prove = []
        self.mining_time = []
        self.current_proposal = []

        # 创建创世块
        self.new_block(previous_hash='1', proof=100)

    def register_node(self, address: str) -> None:
        """
        Add a new node to the list of nodes

        :param address: Address of node. Eg. 'http://192.168.0.5:5000'
        """
        parsed_url = urlparse(address)
        print(parsed_url)
        pattern = re.compile('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d+')
        if re.search(pattern, parsed_url.netloc):
            if parsed_url.netloc not in self.nodes:
                self.nodes.add(parsed_url.netloc)
                with open(os.path.join(BASE_DIR, 'blockchain', 'nodes.txt'), 'w+') as f:
                    if re.search(parsed_url.netloc, f.read()):
                        print('节点已注册')
                    else:
                        f.write(parsed_url.netloc+'\n')
                # if not connect_handle('nodes').find({'node': parsed_url.netloc}):
                #     connect_handle('nodes').insert_one({'node': parsed_url.netloc})
                print(self.nodes)
            else:
                print('节点已注册')
        else:
            return print("未能正确读取信息")

    def mulitiple_registe_node(self, addressList: str) -> None:
        """
        批量添加节点
        :param addressList:传入的需要为存有全部节点的list
        :return:
        """
        try:
            for address in addressList:
                parsed_url = urlparse(address)
                self.nodes.add(parsed_url)
                with open(os.path.join(BASE_DIR, 'blockchain', 'nodes.txt'), 'a') as f:
                    f.write(parsed_url)
        except:
            return print("未能正确读取信息")

    def valid_chain(self, chain: List[Dict[str, Any]]) -> bool:
        """
        Determine if a given blockchain is valid

        :param chain: A blockchain
        :return: True if valid, False if not
        """

        last_block = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]
            # Check that the hash of the block is correct
            if block['previous_hash'] != self.hash(last_block):
                return False

            # Check that the Proof of Work is correct
            if not self.valid_proof(last_block['proof'], block['proof']):
                return False
            last_block = block
            current_index += 1

        return True

    def resolve_conflicts_t(self) -> bool:
        neighbours = self.nodes
        print(neighbours)
        new_chain = None

        # We're only looking for chains longer than ours
        max_length = len(self.chain)
        print(max_length)
        # Grab and verify the chains from all the nodes in our network
        for node in neighbours:
            response = requests.get(f'http://{node}/chain')

            if response.status_code == 200:
                length = response.json()['length']
                print(length)
                chain = response.json()['chain']
                print(chain)

                # Check if the length is longer and the chain is valid
                if length > max_length:
                    max_length = length
                    new_chain = chain

        # Replace our chain if we discovered a new, valid chain longer than ours
        if new_chain:
            self.chain = new_chain
            return True

        return False

    def resolve_conflicts(self) -> bool:
        """
        共识算法解决冲突
        使用网络中最长的链.

        :return:  如果链被取代返回 True, 否则为False

        """
        file_adr = os.path.join(BASE_DIR, 'blockchain_copy', 'chain.pkl')
        file = open(file_adr, 'rb')
        chain = file.read()
        if chain:
            self.chain = pickle.loads(chain)['chain']
        with open(os.path.join(BASE_DIR, 'blockchain', 'nodes.txt'), 'r') as f:
            for line in f:
                self.nodes.add(line.rstrip('\n'))
            neighbours = self.nodes
        '''
        使用ant类来快速获取八个可读取的节点进行同步信息
        该方法暂时注释，当节点量很多时可以采用
        由于该方法是采用蚂蚁寻路的嗅探方法，结合区块链后需要进行多次共识才可以得到一个准确的节点信息
        但可以实现快速的节点信息同步
        ant = ant()
        ant.p_ant(self.nodes)
        neighboures = ant.get_nodes（）

        '''
        new_chain = None
        new_nodes = None

        # We're only looking for chains longer than ours
        max_length = len(self.chain)
        max_nodes = len(self.nodes)
        max_weights = 0
        # Grab and verify the chains from all the nodes in our network
        for node in neighbours:
            # response = requests.get(f'http://{node}/chain')
            try:
                response = requests.get('http://' + node + '/chain', timeout=1)
            except Exception as e:
                print(e)
                continue
            if response.status_code == 200:

                if response.status_code == 200:
                    length = response.json()['length']
                    chain = response.json()['chain']

                    # Check if the length is longer and the chain is valid
                    if length > max_length and self.valid_chain(chain):
                        max_length = length
                        new_chain = chain
                print(response)
                length = pickle.loads(response.content)['length']
                chain = pickle.loads(response.content)['chain']
                last_chain = chain[-1]
                last_proof = last_chain['proof']  # 获取最后一个块的工作量证明
                try:
                    last_recipient = last_chain['transactions'][0]['recipient']
                except IndexError:
                    continue
                # 假设已经得到了mining_time的全部余额
                mining_times = count_somebody_mining_time(last_recipient)
                # 求出权重值
                last_weight = 2 * (mining_times * last_proof) / (mining_times + last_proof)
                nodes = self.nodes
                if last_weight > max_weights:
                    max_weights = last_weight
                    chains = pickle.loads(response.content)['chain']
                    choice = node
                else:
                    choice = 'self'
                    chains = self.chain
                # Check if the length is longer and the chain is valid
                valid_chain_node = self.valid_chain(chain)
                if length > max_length and valid_chain_node:
                    new_chain = chain
                    choice = node
                elif length == max_length and valid_chain_node:
                    new_chain = chains
                if len(nodes) > max_nodes and valid_chain_node:
                    new_nodes = nodes
                print('node:', node, '  length:', length, len(self.chain), ' consensus:', choice, '  valid:', valid_chain_node)
                if new_chain:
                    self.chain = new_chain
                    max_length = len(self.chain)
        # Replace our chain if we discovered a new, valid chain longer than ours

        if new_nodes:
            self.nodes = new_nodes
        if new_chain:
            return True
        return False

    def new_block(self, proof: int, previous_hash: Optional[str]) -> Dict[str, Any]:
        """
        生成新块

        :param proof: The proof given by the Proof of Work algorithm
        :param previous_hash: Hash of previous Block
        :return: New Block
        """
        block = OrderedDict({
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'transactions': self.current_transactions,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
            'companyID': self.companyID,
        })
        # Reset the current list of transactions
        self.current_transactions = []
        self.companyID = []
        self.chain.append(block)
        return block

    def new_transaction(self, sender: str, recipient: str, amount: int, companyID: int, status: str, stage: str) -> int:
        """
        生成新交易信息，信息将加入到下一个待挖的区块中
        :param sender: Address of the Sender
        :param recipient: Address of the Recipient
        :param amount: Amount
        :return: The index of the Block that will hold this transaction
        """

        self.current_transactions.append(OrderedDict({
            'sender': sender,
            'recipient': recipient,
            'amount': amount,
            'timestamp': time(),
            'companyID': companyID,
            'status': status,
            'stage': stage,
        }))
        if companyID not in self.companyID:
            self.companyID.append(companyID)
        return self.last_block['index'] + 1

    @property
    def last_block(self) -> Dict[str, Any]:
        return self.chain[-1]

    @staticmethod
    def hash(block: Dict[str, Any]) -> str:
        """
        生成块的 SHA-256 hash值
        :param block: Block
        """

        # We must make sure that the Dictionary is Ordered, or we'll have inconsistent hashes
        block_string = pickle.dumps(block)
        return hashlib.sha256(block_string).hexdigest()

    def proof_of_work(self, last_proof: int) -> int:
        """
        简单的工作量证明:
         - 查找一个 p' 使得 hash(pp') 以4个0开头
         - p 是上一个块的证明,  p' 是当前的证明
        """

        proof = 0
        while self.valid_proof(last_proof, proof) is False:
            proof += 1

        return proof

    @staticmethod
    def valid_proof(last_proof: int, proof: int) -> bool:
        """
        验证证明: 是否hash(last_proof, proof)以4个0开头

        :param last_proof: Previous Proof
        :param proof: Current Proof
        :return: True if correct, False if not.
        """

        # guess = f'{last_proof}{proof}'.encode()
        guess = str(last_proof).encode() + str(proof).encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:2] == "00"

    # introduction块
    def new_company(self, funder: str, brief: str, award: str, companyID: str, ) -> int:
        self.current_introduction.append(OrderedDict({
            'funder': funder,
            'brief': brief,
            'award': award,
            'timestamp': time(),
            'companyID': companyID,
        }))
        return self.last_block['index'] + 1

    def new_company_block(self, proof: int, previous_hash: Optional[str]) -> Dict[str, Any]:
        """
        生成新块

        :param proof: The proof given by the Proof of Work algorithm
        :param previous_hash: Hash of previous Block
        :return: New Block
        """
        block = OrderedDict({
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'introduction': self.current_introduction,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
            'companyID': self.companyID,
        })

        # Reset the current list of transactions
        self.current_introduction = []
        self.companyID = []
        self.chain.append(block)
        return block

    # 认证信息块
    def new_prove(self, funder_prove: str, company_prove: str, funder: str, companyID: str, ) -> int:
        self.current_prove.append(OrderedDict({
            'funder': funder,
            'companyID': companyID,
            'funder_prove': funder_prove,
            'company_prove': company_prove,
        }))
        self.companyID.append(companyID)
        return self.last_block['index'] + 1

    def new_company_prove(self, proof: int, previous_hash: Optional[str]) -> Dict[str, Any]:
        """
        生成新块

        :param proof: The proof given by the Proof of Work algorithm
        :param previous_hash: Hash of previous Block
        :return: New Block
        """

        block = OrderedDict({
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'company_prove': self.current_prove,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
            'companyID': self.companyID,
        })

        # Reset the current list of transactions
        self.current_prove = []
        self.companyID = []
        self.chain.append(block)
        return block

    def new_mining_time(self, sender: str, recipient: str, mining_time: int) -> int:
        self.current_transactions.append(OrderedDict({
            'sender': sender,
            'recipient': recipient,
            'mining_time': mining_time,
            'timestamp': time(),
        }))
        return self.last_block['index'] + 1

    def new_block_proposal(self, proof: int, previous_hash: Optional[str]) -> Dict[str, Any]:
        block = OrderedDict({
            'index': len(self.chain) + 1,
            'timestamp': time(),
            'proposal': self.current_proposal,
            'proof': proof,
            'previous_hash': previous_hash or self.hash(self.chain[-1]),
            'companyID': self.companyID,
        })

        # Reset the current list of transactions
        self.current_proposal = []
        self.chain.append(block)
        return block

    def new_proposal(self, sender: str, proposal: str, vote_participant: str, oppose_participant: int, companyID: str,
                     flag: str) -> int:
        # 传入的flag是用来表示提议是否通过的状态
        self.current_proposal.append(OrderedDict({
            'sender': sender,
            'proposal': proposal,
            'vote_participant': vote_participant,
            'oppose_participant': oppose_participant,
            'companyID': companyID,
            'flag': flag,
        }))
        if companyID not in self.companyID:
            self.companyID.append(companyID)
        return self.last_block['index'] + 1

    def proofreading(self):
        with open(os.path.join(BASE_DIR, 'blockchain_copy', 'chain.pkl'), 'rb') as f:
            self.chain = pickle.loads(f.read())['chain']
        return self.chain

    def paxos(self, num):
        # 传入的节点数量需为奇数，默认为3
        for i in range(num):
            threads = []
            i = threading.Thread(target=self.resolve_conflicts)
            threads.append(i)
            for t in threads:
                t.setDaemon(False)
                t.start()

    def broadcast(self, previous_hash, ip):
        # 广播给其他已注册节点
        with open(os.path.join(BASE_DIR, 'blockchain', 'nodes.txt'), 'r') as f:
            for line in f:
                self.nodes.add(line.rstrip('\n'))
            neighbours = self.nodes
        for node in neighbours:
            try:
                requests.post('http://' + node + '/make_sure/', {'previous_hash': previous_hash, 'ip': ip}, timeout=1)
            except Exception as e:
                print(e)

    def make_sure(self, previous_hash, from_ip):
        """
        将信息广播给相邻节点，并且验证哈希，如果得到的哈希值得到认同，则返回给需要写入区块的节点一个认同
        :param previous_hash: 得到的哈希值
        :param from_ip: 需要写入区块的ip地址，端口默认8000，如果不是，去utils里修改
        :return:
        """
        last_block = self.chain[-1]
        if mongodb_connection('Hash_cache').find_one({'ip': socket.gethostbyname(socket.gethostname()),
                                                  'hash': previous_hash}):
            return False
        mongodb_connection('Hash_cache').insert_one({'ip': socket.gethostbyname(socket.gethostname()),
                                                 'hash': previous_hash})
        if self.hash(last_block) == previous_hash:
            returns = 1
        else:
            returns = 0
        try:
            requests.post('http://' + from_ip + '/statistics/', {'returns': returns}, timeout=1)
        except Exception as e:
            print(e)
        self.broadcast(previous_hash, from_ip)

if __name__=="__main__":
    blockchain = Blockchain()
    # new_transaction(self, sender: str, recipient: str, amount: int, companyID: int, status: str, stage: str)

    blockchain.register_node("http://127.0.0.1:5000")
    blockchain.register_node("http://127.0.0.1:5001")
    blockchain.new_transaction("wotou","kkk",10,100010,'start','finished')