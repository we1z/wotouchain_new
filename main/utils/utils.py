import os
from wotouchain.wotouchain.settings import BASE_DIR
import pickle
from wotouchain.main.utils.blockchain import *
from wotouchain.main.utils import AES
import random
import zipfile
from pymongo.collection import ObjectId
import json
from uuid import uuid4
import threading


# Generate a globally unique address for this node
node_identifier = str(uuid4()).replace('-', '')

# Instantiate the Blockchain
blockchain = Blockchain()

# 写入区块时返回节点的个数
count_return = 0
# 写入区块时节点认同的个数
count_ensure = 0


def mine():
    # We run the proof of work algorithm to get the next proof...
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)

    # 给工作量证明的节点提供奖励.
    # mining_time作为统计每次挖矿次数的变量，原理与挖矿奖励的代币相同。
    blockchain.new_mining_time(
        sender="wotou",
        recipient=node_identifier,
        mining_time=1,
    )

    # Forge the new Block by adding it to the chain
    # block = blockchain.new_block(proof, None)
    #
    # response = {
    #     'message': "New Block Forged",
    #     'index': block['index'],
    #     'transactions': block['transactions'],
    #     'proof': block['proof'],
    #     'previous_hash': block['previous_hash'],
    #     'comoanyID': block['companyID'],
    # }
    threading.Thread(target=blockchain.broadcast, args=(blockchain.hash(blockchain.chain[-1]),
                                                        socket.gethostbyname(socket.gethostname())+':8000')).start()
    threading.Timer(20, count_node_ensure, args=(proof, 'new_block')).start()
    return True


def mine_prove():
    # We run the proof of work algorithm to get the next proof...
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)

    # 给工作量证明的节点提供奖励.
    # 发送者为 "0" 表明是新挖出的币
    blockchain.new_mining_time(
        sender="wotou",
        recipient=node_identifier,
        mining_time=1,
    )

    # Forge the new Block by adding it to the chain
    # block = blockchain.new_company_prove(proof, None)
    threading.Thread(target=blockchain.broadcast, args=(blockchain.hash(blockchain.chain[-1]),
                                                        socket.gethostbyname(socket.gethostname())+':8000')).start()
    threading.Timer(60, count_node_ensure, args=(proof, 'new_company_prove')).start()
    return True


def mine_proposal():
    # We run the proof of work algorithm to get the next proof...
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)

    # 给工作量证明的节点提供奖励.
    # 发送者为 "0" 表明是新挖出的币
    blockchain.new_mining_time(
        sender="wotou",
        recipient=node_identifier,
        mining_time=1,
    )

    # # Forge the new Block by adding it to the chain
    # block = blockchain.new_block_proposal(proof, None)
    threading.Thread(target=blockchain.broadcast, args=(blockchain.hash(blockchain.chain[-1]),
                                                        socket.gethostbyname(socket.gethostname())+':8000')).start()
    threading.Timer(60, count_node_ensure, args=(proof, 'new_block_proposal')).start()
    return True


def mine_company():
    # We run the proof of work algorithm to get the next proof...
    last_block = blockchain.last_block
    last_proof = last_block['proof']
    proof = blockchain.proof_of_work(last_proof)
    # Forge the new Block by adding it to the chain
    # block = blockchain.new_company_block(proof, None)
    # response = {
    #     'message': "New introduction Forged",
    #     'index': block['index'],
    #     'introduction': block['introduction'],
    #     'proof': block['proof'],
    #     'previous_hash': block['previous_hash'],
    #     'comoanyID':block['companyID'],
    # }
    threading.Thread(target=blockchain.broadcast, args=(blockchain.hash(blockchain.chain[-1]),
                                                        socket.gethostbyname(socket.gethostname())+':8000')).start()
    threading.Timer(60, count_node_ensure, args=(proof, 'new_company_block')).start()
    return True


def new_transaction(values):
    # values = request.POST.get('data')
    # 检查POST数据
    required = ['sender', 'recipient', 'amount','companyID','status','stage']
    if not all(k in values for k in required):
        return 'Missing values', 400

    # Create a new Transaction
    index = blockchain.new_transaction(values['sender'], values['recipient'], values['amount'], values['companyID'], values['status'], values['stage'])
    response = {'message': 'Transaction will be added to Block ' + str(index)}
    return response


def new_company_add(values):
    # values = request.POST.get('data')
    # 目前填入项目数据不会提供奖励，因为这个只能咱们来填，这个是预留的一个后门
    # 检查POST数据
    required = ['funder', 'brief', 'award','companyID']
    if not all(k in values for k in required):
        return 'Missing values', 400

    # Create a new Transaction
    index = blockchain.new_company(values['funder'], values['brief'], values['award'], values['companyID'])

    response = {'message': 'introduction will be added to Block ' + str(index)}
    return response


def new_prove_add(values):
    # values = request.POST.get('data')
    # 检查POST数据
    required = ['funder', 'companyID', 'funder_prove','company_prove']
    if not all(k in values for k in required):
        return 'Missing values', 400

    # Create a new Transaction
    index = blockchain.new_prove(values['funder_prove'], values['company_prove'], values['funder'],  values['companyID'])

    response = {'message': 'prove_information will be added to Block ' + str(index)}
    return response


def new_proposal_add(values):
    # 检查POST数据
    required = ['sender', 'proposal', 'vote_participant','oppose_participant', 'companyID', 'flag']
    if not all(k in values for k in required):
        return 'Missing values', 400

    # Create a new Transaction
    index = blockchain.new_proposal(values['funder_prove'], values['company_prove'], values['funder'],  values['companyID'])

    response = {'message': 'prove_information will be added to Block ' + str(index)}
    return response


def full_chain():
    # 查看所有区块
    response = {
        'chain': blockchain.proofreading(),
        'length': len(blockchain.proofreading()),
    }
    return response


def register_nodes(values):
    nodes = values.get('nodes')
    print(nodes)
    if nodes is None:
        return "Error: Please supply a valid list of nodes", 400

    # for node in nodes:
    blockchain.register_node(nodes)

    response = {
        'message': 'New nodes have been added',
        'total_nodes': list(blockchain.nodes),
    }
    return response


def consensus():
    replaced = blockchain.resolve_conflicts()
    if replaced:
        response = {
            'message': 'Our chain was replaced',
            'new_chain': blockchain.chain
        }
        chain = {
            'chain': blockchain.chain,
            'length': len(blockchain.chain),
        }
        with open(os.path.join(BASE_DIR, 'blockchain_copy', 'chain.pkl'), 'wb') as f:
            f.write(pickle.dumps(chain))
    else:
        response = {
            'message': 'Our chain is authoritative',
            'chain': blockchain.chain
        }
    return response


def make_sures(previous_hash, from_ip):
    """
    确认其他节点发过来的哈希值是否等于自己最后一区块的哈希值，并且将得到的哈希值继续广播给相邻节点
    :param previous_hash:
    :param from_ip:
    :return:
    """
    threading.Thread(target=blockchain.make_sure, args=(previous_hash, from_ip)).start()
    return True


def statistic(returns):
    """
    每收到一次认同，统计一次
    :param returns:
    :return:
    """
    global count_ensure, count_return
    count_return += 1
    count_ensure += int(returns)


def count_node_ensure(proof, execute):
    """
    统计写入区块收到的认同是否到达50%，如果达到写入区块链
    :param proof:
    :param execute:
    :return:
    """
    global count_return, count_ensure
    if count_return:
        if count_ensure / count_return >= 0.5:
            fun = getattr(blockchain, execute)
            fun(proof, None)
            verify_chain()
            count_return = 0
            count_ensure = 0
            return True
        else:
            count_return = 0
            count_ensure = 0
            return False
    return False


def file_iter(path, chunck_size):
    # 返回文件流
    with open(os.path.join(BASE_DIR, path), 'rb') as f:
        while True:
            c = f.read(chunck_size)
            if c:
                yield c
            else:
                break


def verify_chain():
    """
    验证备份区块链和内存内的区块链是否相同
    :return:
    """
    chain = blockchain.chain
    file_adr = os.path.join(BASE_DIR, 'blockchain_copy', 'chain.pkl')
    file = open(file_adr, 'rb')
    data = file.read()
    if data:
        file_chain = pickle.loads(data)
        print(len(file_chain['chain']), len(chain))
        if file_chain['chain'] == chain:
            return True, file.close()
        elif len(file_chain['chain']) > len(chain):
            blockchain.chain = file_chain['chain']
        else:
            download_chain(file_adr)
    else:
        download_chain(file_adr)


def download_chain(file_adr):
    chain = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain),
    }
    file = open(file_adr, 'wb')
    file.write(pickle.dumps(chain))
    file.close()


# def imange_to_binary(path):
#     """
#     :param path:文件路径
#     :return:
#     """
#     image = cv2.imread(path)
#     rows = image.shape[0]
#     cols = image.shape[1]
#     #把图像转换为二进制文件
#     #python写二进制文件，f = open('name','wb')
#     #只有wb才是写二进制文件
#     fileSave = open('patch.bin','wb')
#     for step in range(0,rows):
#         for step2 in range(0,cols):
#             fileSave.write(image[step,step2,2])
#     for step in range(0,rows):
#         for step2 in range(0,cols):
#             fileSave.write(image[step,step2,1])
#     for step in range(0,rows):
#         for step2 in range(0,cols):
#             fileSave.write(image[step,step2,0])
#     fileSave.close()

def file_to_dict(path):
    """
    :param path:文件夹路径
    :return: dict， key
    """
    key = random_key()
    dic = {}
    lis = os.listdir(path)
    file_name = None
    for i in range(0, len(lis)):
        if os.path.splitext(lis[i])[1] == '.zip':
            file_zip = zipfile.ZipFile(os.path.join(path, lis[i]), 'r')
            for file in file_zip.namelist():
                file_zip.extract(file, path)
            file_zip.close()
            file_name = os.path.splitext(lis[i])[0]
            os.remove(os.path.join(path, lis[i]))
    path = os.path.join(path, file_name)
    lis = os.listdir(path)
    for i in range(0, len(lis)):
        file_adr = os.path.join(path, lis[i])
        with open(file_adr, 'rb') as f:
            data = f.read()
        code = AES.PrpCrypt(key)
        data = code.encrypt(data)
        file = lis[i]
        dic[file] = data
    return dic, key


# def dic_to_file(key, dic, pro_name):
#     """
#     :param dic:字典
#     :param key:密钥
#     :param pro_name:文件名
#     :return:
#     通过dic创建文件
#     """
#     path = os.path.join(BASE_DIR, 'file_pag', pro_name)
#     zip_name = os.path.splitext(models.Projects.objects.get(name=pro_name).file_adr.split('\\')[1])[0]
#     try:
#         os.makedirs(path)
#     except Exception as e:
#         print(e)
#     print(dic)
#     for i, v in dic.items():
#         code = AES.PrpCrypt(key)
#         file = i
#         data = code.decrypt(v)
#         file_adr = os.path.join(path, file)
#         with open(file_adr, 'wb') as f:
#             f.write(data)
#     f = zipfile.ZipFile(os.path.join(path, zip_name+'.zip'), 'w', zipfile.ZIP_DEFLATED)
#     print(path)
#     if os.path.isdir(path):
#         for d in os.listdir(path):
#             if os.path.splitext(d)[1] != '.zip':
#                 f.write(os.path.join(path, d), d)
#     f.close()
#     for i in os.listdir(path):
#         if os.path.splitext(i)[1] != '.zip':
#             os.remove(os.path.join(path, i))


def random_key():
    """
    :return:随机16位密钥
    """
    code = ''
    for i in range(16):
        current = random.randrange(0, 16)
        if current != i and current % 2 == 0:
            temp = random.randint(0, 9)
        else:
            temp = chr(random.randint(65, 90))
        code += str(temp)
    return code


def encrp(data):
    """
    加密信息
    :param data:
    :return: {'info': 加密后的数据} , 密钥
    """
    key = random_key()
    code = AES.PrpCrypt(key)
    data = code.encrypt(data)
    dic = {'info': data}
    return dic, key


class MyEncoder(json.JSONEncoder):
    """
    json.dumps自定义类
    """
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        if isinstance(obj, bytes):
            return str(obj)
        return json.JSONEncoder.default(self, obj)