from threading import Thread
import subprocess
from queue import *
from User.utils import *


class Ping:
    def __init__(self):
        self.num_threads=3
        self.ips=[]
        self.q=Queue()
        self.ip_list = []

    def get_ips(self, ips):
        self.ips = ips

    def pingme(self,i,queue):
        while True:
            ip=queue.get()
            print( 'Thread %s pinging %s' %(i,ip))
            ret=subprocess.call('ping -c 1 %s' % ip,shell=True,stdout=open('/dev/null','w'),stderr=subprocess.STDOUT)
            if ret==0:
                print( '%s is alive!' %ip  )
                self.ip_list.append(ip)
            elif ret==1:
                print ('%s is down...'%ip  )
                return self.ip_list
            queue.task_done()

    def run_threads(self):
        for i in range(self.num_threads):
            t = Thread(target=self.pingme, args=(i, q))
            t.setDaemon(True)
            t.start()

        for ip in self.ips:
            self.q.put(ip)
            print('main thread waiting...')
            self.q.join()
            print('Done')