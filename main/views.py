from __future__ import unicode_literals
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import json
from .db_connection import mongodb_connection
import time
import os
from datetime import datetime
import hashlib
from django.http import HttpResponseRedirect, HttpResponse, FileResponse
import random
import smtplib
from email.mime.text import MIMEText
from email.header import Header
import re
from wotouchain.settings import BASE_DIR


def md5(string):
    """
    md5加密
    :param string: 账号和密码
    :return: 加密后的账号和密码
    """
    hl = hashlib.md5()
    hl.update(string.encode(encoding='utf-8'))
    return hl.hexdigest()

def get_ticket():
    """
    生成cookies
    :return: 绑定到浏览器上的cookies
    """
    string = str(time.time() - random.randint(0, 10000))
    sha256 = hashlib.sha256()
    sha256.update(string.encode('utf-8'))
    res = sha256.hexdigest()
    return res

def hash(string):
    """
    生成cookies
    :param string:
    :return:
    """
    sha256 = hashlib.sha256()
    sha256.update(string.encode('utf-8'))
    res = sha256.hexdigest()
    return res

def login_auth(func):
    """
    装饰器，验证普通用户登录
    :param func:
    :return:
    """
    def inner(request):
        ticket = request.COOKIES.get('ticket')
        username = request.COOKIES.get('udfuenf')
        password = request.COOKIES.get('pdfuenf')
        if not ticket:
            return func(request)
        elif not username:
            return func(request)
        elif not password:
            return func(request)
        if mongodb_connection('server')['wotouchain_user'].count(
                {'ticket': ticket, 'username': username, 'password': password}) > 0:
            if time.time() > mongodb_connection('server')['wotouchain_user'].find_one(
                    {'ticket': ticket, 'username': username, 'password': password})['cookies_expired']:
                return func(request)
            else:
                return func(request, auth=True)
        else:
            return func(request)

    return inner

def admin_auth(request):
    """
    装饰器，验证管理员登录
    :param request:
    :return:
    """
    ticket = request.COOKIES.get('ticket')
    username = request.COOKIES.get('udfuenf')
    password = request.COOKIES.get('pdfuenf')
    if not ticket:
        return False
    elif not username:
        return False
    elif not password:
        return False
    if mongodb_connection('server')['wotouchain_administrator'].count(
            {'ticket': ticket, 'username': username, 'password': password}) > 0:
        if time.time() > mongodb_connection('server')['wotouchain_administrator'].find_one(
                {'ticket': ticket, 'username': username, 'password': password})['cookies_expired']:
            return False
        else:
            return True
    else:
        return False

@login_auth
def home(request, auth=False):
    """
    主页
    :param request:
    :param auth:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'home.html', {'auth': auth})


def is_contain_chinese(check_str):
    """
    判断字符串中是否包含中文
    :param check_str: {str} 需要检测的字符串
    :return: {bool} 包含返回True， 不包含返回False
    """
    for ch in check_str:
        if u'\u4e00' <= ch <= u'\u9fff':
            pass
        else:
            return True
    return False


def check_id_num(id_num):
    """
    检查身份证号码
    :param id_num:
    :return:
    """
    for i in id_num[:-1]:
        if i not in '1234567890':
            return True
    return False


@csrf_exempt
def register(request):
    """
    新用户注册页
    :param request:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'register.html')
    elif request.method == 'POST':
        email = request.POST.get('email')
        if 'check_email' in request.POST:
            email = email.strip()
            if len(email) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': ''}))
            if len(email) < 5:
                return HttpResponse(json.dumps({'type': 1, 'msg': '邮箱太短'}))
            if '@' not in email or '.' not in email:
                return HttpResponse(json.dumps({'type': 1, 'msg': '邮箱格式错误'}))
            if mongodb_connection('server')['wotouchain_user'].count({'email': email}) > 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': '邮箱已被注册'}))
            else:
                return HttpResponse(json.dumps({'type': 0, 'msg': '验证通过'}))
        password = request.POST.get('password')
        if 'check_password' in request.POST:
            if len(password) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': ''}))
            if len(password) < 8:
                return HttpResponse(json.dumps({'type': 1, 'msg': '密码太短'}))
            if len(password) > 20:
                return HttpResponse(json.dumps({'type': 1, 'msg': '密码太长'}))
            else:
                return HttpResponse(json.dumps({'type': 0, 'msg': '验证通过'}))
        repeat_password = request.POST.get('repeat_password')
        if 'check_repeat_password' in request.POST:
            if len(repeat_password) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': ''}))
            if repeat_password != password:
                return HttpResponse(json.dumps({'type': 1, 'msg': '密码不一致'}))
            else:
                return HttpResponse(json.dumps({'type': 0, 'msg': '验证通过'}))
        nick_name = request.POST.get('nick_name')
        if 'check_nick_name' in request.POST:
            if len(nick_name) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': ''}))
            if mongodb_connection('server')['wotouchain_user'].count({'nick_name': nick_name}) > 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': '昵称已被注册'}))
            else:
                return HttpResponse(json.dumps({'type': 0, 'msg': '验证通过'}))
        real_name = request.POST.get('real_name')
        if 'check_real_name' in request.POST:
            if len(real_name) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': ''}))
            if is_contain_chinese(real_name):
                return HttpResponse(json.dumps({'type': 1, 'msg': '真实姓名必须全部为中文'}))
            else:
                return HttpResponse(json.dumps({'type': 0, 'msg': '验证通过'}))
        id_num = request.POST.get('id_num')
        if 'check_id_num' in request.POST:
            if len(id_num) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': ''}))
            if len(id_num) != 18:
                return HttpResponse(json.dumps({'type': 1, 'msg': '身份证长度错误'}))
            if check_id_num(id_num):
                return HttpResponse(json.dumps({'type': 1, 'msg': '身份证格式错误'}))
            else:
                return HttpResponse(json.dumps({'type': 0, 'msg': '验证通过'}))
        tel_num = request.POST.get('tel_num')
        if 'check_tel_num' in request.POST:
            if len(tel_num) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': ''}))
            if len(tel_num) != 11:
                return HttpResponse(json.dumps({'type': 1, 'msg': '手机号码长度错误'}))
            if check_id_num(tel_num):
                return HttpResponse(json.dumps({'type': 1, 'msg': '手机号码必须是纯数字'}))
            else:
                return HttpResponse(json.dumps({'type': 0, 'msg': '验证通过'}))
        username = md5(email)
        register_time = time.time()
        password = md5(password)
        mongodb_connection('server')['wotouchain_user'].insert({'email': email,
                                                                'password': password,
                                                                'nick_name': nick_name,
                                                                'real_name': real_name,
                                                                'id_num': id_num,
                                                                'tel_num': tel_num,
                                                                'username': username,
                                                                'register_time': register_time,
                                                                'auth': '未激活',
                                                                'audit': '未审核'})
        send_verify_code(email, username)
        return render(request, 'home.html')


def send_verify_code(email, email_encode):
    """
    发送注册验证邮件
    :param email:
    :param email_encode:
    :return:
    """
    sender = 'zhangyanping0519@126.com'
    receiver = email
    subject = '窝头科技——注册验证'
    smtpserver = 'smtp.126.com'
    username = 'zhangyanping0519@126.com'
    password = 'lf720823'
    register_verify_code = md5(str(time.time()))
    msg = MIMEText("""
            <div>
            <h2>窝头科技区块链记账系统</h2>
            <p>点击以下链接完成验证：</P>
            <a href="http://106.75.91.135:8888/register_verify?rvc={}&uid={}">http://106.75.91.135:8888/register_verify?rvc={}&uid={}</a>
            </div>
    """.format(register_verify_code, email_encode, register_verify_code, email_encode), 'HTML', 'utf-8')
    msg['Subject'] = Header(subject, 'utf-8')
    msg['from'] = 'zhangyanping0519@126.com'
    msg['to'] = email
    smtp = smtplib.SMTP()
    smtp.connect(smtpserver)
    smtp.login(username, password)
    smtp.sendmail(sender, receiver, msg.as_string())
    smtp.quit()

    mongodb_connection('server')['wotouchain_user'].update({'email': email, 'username': email_encode}, {
        '$set': {'register_verify_code': register_verify_code}})


def send_reset_pd_verify_code(email, email_encode):
    """
    发送重设密码验证邮件
    :param email:
    :param email_encode:
    :return:
    """
    sender = 'zhangyanping0519@126.com'
    receiver = email
    subject = '窝头科技——找回密码'
    smtpserver = 'smtp.126.com'
    username = 'zhangyanping0519@126.com'
    password = 'lf720823'
    resetpd_verify_code = md5(str(time.time()))
    resetpd_verify_code_expired = time.time() + 900
    msg = MIMEText("""
            <div>
            <h2>窝头科技区块链记账系统</h2>
            <p>点击以下链接重置密码：</P>
            <a href="http://106.75.91.135:8888/reset_pd?spvc={}&uid={}">http://106.75.91.135:8888/reset_pd?spvc={}&uid={}</a>
            <p>该链接15分钟内有效</P>
            </div>
    """.format(resetpd_verify_code, email_encode, resetpd_verify_code, email_encode), 'HTML', 'utf-8')
    msg['Subject'] = Header(subject, 'utf-8')
    msg['from'] = 'zhangyanping0519@126.com'
    msg['to'] = email
    smtp = smtplib.SMTP()
    smtp.connect(smtpserver)
    smtp.login(username, password)
    smtp.sendmail(sender, receiver, msg.as_string())
    smtp.quit()

    mongodb_connection('server')['wotouchain_user'].update({'email': email, 'username': email_encode}, {
        '$set': {'resetpd_verify_code': resetpd_verify_code,
                 'resetpd_verify_code_expired': resetpd_verify_code_expired}})


def register_verify(request):
    """
    注册验证，账号激活页面
    :param request:
    :return:
    """
    if request.method == 'GET':
        register_verify_code = request.GET.get('rvc')
        username = request.GET.get('uid')
        if mongodb_connection('server')['wotouchain_user'].count(
                {'username': username, 'register_verify_code': register_verify_code}) != 1:
            pass
        else:
            mongodb_connection('server')['wotouchain_user'].update(
                {'username': username, 'register_verify_code': register_verify_code}, {'$set': {'auth': True}})
            return HttpResponseRedirect('log_in.html')


@csrf_exempt
def test(request):
    return render(request, 'test.html')


@csrf_exempt
def upload_file(request):
    # 拿到文件，保存在指定路径
    imgFile = request.FILES.get('upload')
    filename = str(float(time.time()))
    if os.path.exists('/data/wotouchain/project_pic/'):
        pass
    else:
        os.mkdir('/data/wotouchain/project_pic/')
    with open('/data/wotouchain/project_pic/{}'.format(filename), 'wb')as f:
        for chunk in imgFile.chunks():
            f.write(chunk)
    response = {
        "uploaded": 1,
        "fileName": filename,
        "url": 'http://106.75.91.135/wotouchain/project_pic/{}'.format(filename)
    }
    return HttpResponse(json.dumps(response))


@csrf_exempt
def log_in(request):
    """
    登录
    :param request:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'log_in.html')
    elif request.method == 'POST':
        username = md5(request.POST.get('email'))
        password = md5(request.POST.get('password'))
        if mongodb_connection('server')['wotouchain_user'].count({'username': username, 'password': password}) == 0:
            return HttpResponse(json.dumps({'type': 1, 'msg': '账号或密码错误'}))
        else:
            ticket = get_ticket()
            # 绑定令牌到cookie里面
            response = HttpResponse(json.dumps({'type': 0, 'msg': '登录成功'}))
            response.set_cookie('ticket', ticket, max_age=100000)
            response.set_cookie('pdfuenf', password, max_age=100000)
            response.set_cookie('udfuenf', username, max_age=100000)
            # 存在服务端
            mongodb_connection('server')['wotouchain_user'].update({'username': username, 'password': password}, {
                '$set': {'ticket': ticket, 'cookies_expired': time.time() + 3600 * 8 * 7}})
            return response


@csrf_exempt
def forget_pd(request):
    """
    忘记密码
    :param request:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'forget_pd.html')
    elif request.method == 'POST':
        email = request.POST.get('email')
        username = md5(email)
        real_name = request.POST.get('real_name')
        id_num = request.POST.get('id_num')
        tel_num = request.POST.get('tel_num')
        if mongodb_connection('server')['wotouchain_user'].count(
                {'username': username, 'real_name': real_name, 'id_num': id_num, 'tel_num': tel_num}) == 0:
            if mongodb_connection('server')['wotouchain_user'].count(
                    {'username': username, 'real_name': real_name}) == 0:
                return HttpResponse(json.dumps({'type': 1, 'msg': '注册邮箱或者真实姓名错误'}))
            else:
                return HttpResponse(json.dumps({'type': 1, 'msg': '身份证号或手机号码错误'}))
        else:
            send_reset_pd_verify_code(email, username)
            return HttpResponse(json.dumps({'type': 0, 'msg': '验证邮件已发送，请登录邮箱完成密码重置'}))


@csrf_exempt
def reset_pd(request):
    """
    重设密码
    :param request:
    :return:
    """
    if request.method == 'GET':
        username = request.GET.get('uid')
        verify_code = request.GET.get('spvc')
        return render(request, 'reset_pd.html', {'username': username, 'verify_code': verify_code})
    elif request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        repeat_password = request.POST.get('repeat_password')
        username = request.POST.get('username')
        verify_code = request.POST.get('verify_code')
        now = time.time()
        if mongodb_connection('server')['wotouchain_user'].count(
                {'username': username, 'email': email, 'resetpd_verify_code': verify_code}) == 0:
            return HttpResponse(json.dumps({'type': 1, 'msg': '注册邮箱错误或验证链接非法'}))
        if mongodb_connection('server')['wotouchain_user'].find_one(
                {'username': username, 'email': email, 'resetpd_verify_code': verify_code})[
            'resetpd_verify_code_expired'] < now:
            return HttpResponse(json.dumps({'type': 1, 'msg': '验证链接已过期'}))
        if len(password) < 8:
            return HttpResponse(json.dumps({'type': 1, 'msg': '密码太短'}))
        if len(password) > 20:
            return HttpResponse(json.dumps({'type': 1, 'msg': '密码天长'}))
        if password != repeat_password:
            return HttpResponse(json.dumps({'type': 1, 'msg': '密码不一致'}))
        password_encode = md5(password)
        mongodb_connection('server')['wotouchain_user'].update(
            {'username': username, 'email': email, 'resetpd_verify_code': verify_code},
            {'$set': {'password': password_encode}})
        return HttpResponse(json.dumps({'type': 0, 'msg': '密码重置成功，请重新登录'}))


@login_auth
def user_page(request, auth=False):
    """
    用户个人页面
    :param request:
    :param auth:
    :return:
    """
    if auth:
        username = request.COOKIES.get('udfuenf')
        password = request.COOKIES.get('pdfuenf')
        project_list = [i for i in mongodb_connection('server')['wotouchain_project'].find(
            {'username': username, 'password': password})]
        return render(request, 'user_page.html', {'project_list': project_list})
    else:
        return HttpResponseRedirect('log_in.html')


@login_auth
@csrf_exempt
def start_crowdfunding(request, auth=False):
    """
    发起众筹页
    :param request:
    :param auth:
    :return:
    """
    if auth:
        if request.method == 'GET':
            I = str(time.time()).split('.')
            pi = md5(I[0]) + '-' + md5(I[1])
            respnse = render(request, 'start_crowdfunding.html')
            respnse.delete_cookie('pi')
            respnse.set_cookie('pi', pi, max_age=100000)
            return respnse
        elif request.method == 'POST':
            project_name = request.POST.get('project_name')
            brief_summary = request.POST.get('brief_summary')
            target_amount = request.POST.get('target_amount')
            deadline = request.POST.get('deadline')
            email = request.POST.get('email')
            tel = request.POST.get('tel')
            Alipay = request.POST.get('Alipay')
            wechat = request.POST.get('wechat')
            bank_account = request.POST.get('bank_account')
            self_introduction = request.POST.get('self_introduction')
            project_introduction = request.POST.get('project_introduction')
            reward = request.POST.get('reward')
            unit_price = request.POST.get('unit_price')
            thanks = request.POST.get('thanks')
            post_time = datetime.now().strftime("%Y-%m-%d")
            project_index = request.COOKIES.get('pi')

            username = request.COOKIES.get('udfuenf')
            password = request.COOKIES.get('pdfuenf')

            pg_1 = request.POST.get('pg_1')
            pw_1 = request.POST.get('pw_1')
            pg_2 = request.POST.get('pg_2')
            pw_2 = request.POST.get('pw_2')
            pg_3 = request.POST.get('pg_3')
            pw_3 = request.POST.get('pw_3')
            pg_4 = request.POST.get('pg_4')
            pw_4 = request.POST.get('pw_4')
            pg_5 = request.POST.get('pg_5')
            pw_5 = request.POST.get('pw_5')
            pg_6 = request.POST.get('pg_6')
            pw_6 = request.POST.get('pw_6')
            pg_7 = request.POST.get('pg_7')
            pw_7 = request.POST.get('pw_7')
            pg_8 = request.POST.get('pg_8')
            pw_8 = request.POST.get('pw_8')
            pg_9 = request.POST.get('pg_9')
            pw_9 = request.POST.get('pw_9')
            pg_10 = request.POST.get('pg_10')
            pw_10 = request.POST.get('pw_10')

            if 'picpath' in request.FILES:
                cover_path = 'http://106.75.91.135/wotouchain/project_cover/{}'.format(project_index) + '.png'
                save_path = '/data/wotouchain/project_cover/{}'.format(project_index) + '.png'
                img = request.FILES.get('picpath')
                with open(save_path, 'wb') as f:
                    for chunk in img.chunks(chunk_size=1024):
                        f.write(chunk)
            else:
                cover_path = None

            project_data = {
                'project_name': project_name,
                'brief_summary': brief_summary,
                'target_amount': target_amount,
                'deadline': deadline,
                'email': email,
                'tel': tel,
                'Alipay': Alipay,
                'wechat': wechat,
                'bank_account': bank_account,
                'self_introduction': self_introduction,
                'project_introduction': project_introduction,
                'reward': reward,
                'unit_price': unit_price,
                'thanks': thanks,
                'username': username,
                'password': password,
                'cover_path': cover_path,
                'Auditing': '未审核',
                'post_time': post_time,
                'project_index': project_index,
                'project_phase': [
                    {
                        'pg_1': pg_1,
                        'pw_1': pw_1
                    },
                    {
                        'pg_2': pg_2,
                        'pw_2': pw_2
                    },
                    {
                        'pg_3': pg_3,
                        'pw_3': pw_3
                    },
                    {
                        'pg_4': pg_4,
                        'pw_4': pw_4
                    },
                    {
                        'pg_5': pg_5,
                        'pw_5': pw_5
                    },
                    {
                        'pg_6': pg_6,
                        'pw_6': pw_6
                    },
                    {
                        'pg_7': pg_7,
                        'pw_7': pw_7
                    },
                    {
                        'pg_8': pg_8,
                        'pw_8': pw_8
                    },
                    {
                        'pg_9': pg_9,
                        'pw_9': pw_9
                    },
                    {
                        'pg_10': pg_10,
                        'pw_10': pw_10
                    }
                ]
            }
            if mongodb_connection('server')['wotouchain_project'].count(
                    {'username': username, 'project_index': project_index}) == 0:
                mongodb_connection('server')['wotouchain_project'].insert(project_data)
            else:
                mongodb_connection('server')['wotouchain_project'].update(
                    {'username': username, 'project_index': project_index}, {'$set': project_data})
            return HttpResponseRedirect('user_page.html')
    else:
        return HttpResponseRedirect('log_in.html')


@csrf_exempt
def auto_save(request):
    if request.method == 'POST':
        self_introduction = request.POST.get('self_introduction')
        if mongodb_connection('server')['test'].count({'id': 1}) != 0:
            mongodb_connection('server')['test'].update({'id': 1}, {'$set': {'self_introduction': self_introduction}})
        else:
            mongodb_connection('server')['test'].insert({'id': 1, 'self_introduction': self_introduction})
        return HttpResponse(json.dumps({'msg': '自动保存成功'}))


@csrf_exempt
def logout(request):
    """
    用户注销，删除cookies
    :param request:
    :return:
    """
    if request.method == 'GET':
        response = HttpResponseRedirect('home.html')
        response.delete_cookie('ticket')
        response.delete_cookie('pdfuenf')
        response.delete_cookie('udfuenf')
        response.delete_cookie('pi')

        return response

@admin_auth
@csrf_exempt
def administrator(request):
    """
    管理员控制台登录页
    :param request:
    :return:
    """
    if request.method == 'GET':
        return render(request, 'administrator.html')
    if request.method == 'POST':
        username = md5(request.POST.get('inputUsername'))
        password = md5(request.POST.get('inputPassword'))
        if mongodb_connection('server')['wotouchain_administrator'].count(
                {'username': username, 'password': password}) == 0:
            return render(request, 'home.html')
        else:
            ticket = get_ticket()
            # 绑定令牌到cookie里面
            response = HttpResponseRedirect('console.html')
            response.set_cookie('ticket', ticket, max_age=100000)
            response.set_cookie('apdfuenf', password, max_age=100000)
            response.set_cookie('audfuenf', username, max_age=100000)
            # 存在服务端
            mongodb_connection('server')['wotouchain_administrator'].update(
                {'username': username, 'password': password}, {
                    '$set': {'aticket': ticket, 'cookies_expired': time.time() + 3600 * 8 * 7}})
            return response


@csrf_exempt
def console(request):
    """
    控制台主页（非动态加载）
    :param request:
    :return:
    """
    if request.method == 'GET':
        username = request.COOKIES.get('audfuenf')
        password = request.COOKIES.get('apdfuenf')
        if mongodb_connection('server')['wotouchain_administrator'].count(
                {'username': username, 'password': password}) == 1:
            return render(request, 'console.html')
        else:
            return render(request, 'home.html')


def console_contain(request):
    """
    管理员控制台处理数据的页面，eg.节点注册
    :param request:
    :return:
    """
    if request.method == 'POST':
        event = request.POST.get('event')
        admin = request.COOKIES.get('audfuenf')
        if event == 'register_node':
            node = request.POST.get('event_value')
            if re.match("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,5}", node):
                try:
                    with open(BASE_DIR + '/blockchain/node', 'r') as f:
                        node_info = eval(f.read())
                    node_list = [i['node'] for i in node_info]
                    if node in node_list:
                        return HttpResponse(json.dumps({'msg': '节点已被注册！'}))
                    else:
                        node_info.append({'node': node, 'time': time.time(), 'admin': admin})
                        with open(BASE_DIR + '/blockchain/node', 'w') as f:
                            f.write(str(node_info))
                        mongodb_connection('server')['wotouchain_nodes'].remove()
                        mongodb_connection('server')['wotouchain_nodes'].insert_many(node_info)
                        return HttpResponse(json.dumps({'msg': '节点注册成功！'}))
                except:
                    node_info = []
                    node_info.append({'node': node, 'time': time.time(), 'admin': admin})
                    with open(BASE_DIR + '/blockchain/node', 'w') as f:
                        f.write(str(node_info))
                    mongodb_connection('server')['wotouchain_nodes'].remove()
                    mongodb_connection('server')['wotouchain_nodes'].insert_many(node_info)
                    return HttpResponse(json.dumps({'msg': '节点注册成功！'}))
            else:
                return HttpResponse(json.dumps({'msg': '节点格式错误！'}))


def console_main(request):
    """
    管理员控制台动态加载的主页
    :param request:
    :return:
    """
    return render(request, 'console-main.html')


def console_nodes(request):
    """
    管理员控制台动态加载的节点注册页面
    :param request:
    :return:
    """
    node_info = [i for i in mongodb_connection('server')['wotouchain_nodes'].find()]
    return render(request, 'console-nodes.html', {'node_info': node_info})


def console_audit(request):
    """
    管理员控制台动态加载的项目审核页面
    :param request:
    :return:
    """
    project_list = [i for i in mongodb_connection('server')['wotouchain_project'].find({"Auditing": "未审核"})]
    return render(request, 'console-audit.html', {'project_list': project_list})


def console_user_audit(request):
    """
    管理员控制台动态加载的用户审核页面
    :param request:
    :return:
    """
    if request.method == 'GET':
        user_list = [i for i in mongodb_connection('server')['wotouchain_user'].find({"audit": "审核中"})]
        return render(request, 'console-user-audit.html', {'user_list': user_list})
    elif request.method == 'POST':
        id = request.POST.get('id')
        if request.POST.get('state') == '1':
            mongodb_connection('server')['wotouchain_user'].update({'username': id}, {'$set': {'audit': '通过审核'}})
        if request.POST.get('state') == '0':
            mongodb_connection('server')['wotouchain_user'].update({'username': id}, {'$set': {'audit': '审核失败'}})
        return HttpResponse(json.dumps({'c': 'close-{}'.format(id), 'a': 'audit-{}'.format(id)}))


def user_main(request):
    """
    用户个人页主页
    :param request:
    :return:
    """
    username = request.COOKIES.get('udfuenf')
    password = request.COOKIES.get('pdfuenf')
    project_list = [i for i in mongodb_connection('server')['wotouchain_project'].find(
        {'username': username, 'password': password})]
    return render(request, 'user-main.html', {'project_list': project_list})


def user_profile(request):
    """
    动态加载的用户详情页
    :param request:
    :return:
    """
    if request.method == 'GET':
        username = request.COOKIES.get('udfuenf')
        password = request.COOKIES.get('pdfuenf')
        profile = mongodb_connection('server')['wotouchain_user'].find_one({'username': username, 'password': password})
        return render(request, 'user-profile.html', profile)
    elif request.method == 'POST':
        username = request.COOKIES.get('udfuenf')
        password = request.COOKIES.get('pdfuenf')
        if 'nick_name' in request.POST:
            nick_name = request.POST.get('nick_name')
            if len(nick_name) > 6:
                return HttpResponse(json.dumps({'msg': '昵称太长！'}))
            elif len(nick_name) == 0:
                return HttpResponse(json.dumps({'msg': '昵称不能为空！'}))
            else:
                mongodb_connection('server')['wotouchain_user'].update({'username': username, 'password': password},
                                                                       {'$set': {'nick_name': nick_name}})
                return HttpResponse(json.dumps({'msg': 1}))
        elif 'tel_num' in request.POST:
            tel_num = request.POST.get('tel_num')
            for i in tel_num:
                if i not in '1234567890':
                    return HttpResponse(json.dumps({'msg': '手机号码只能为数字！'}))
            if len(tel_num.strip()) != 11:
                return HttpResponse(json.dumps({'msg': '手机号码位数错误！'}))
            else:
                mongodb_connection('server')['wotouchain_user'].update({'username': username, 'password': password},
                                                                       {'$set': {'tel_num': tel_num}})
                return HttpResponse(json.dumps({'msg': 1}))
        elif 'email_bak' in request.POST:
            email_bak = request.POST.get('email_bak')
            if not re.match('^[0-9a-zA-Z\_]{1,30}@[0-9a-zA-Z\_\-\.]{1,30}\.[a-zA-Z]{2,5}', email_bak):
                return HttpResponse(json.dumps({'msg': '邮箱格式错误！'}))
            else:
                mongodb_connection('server')['wotouchain_user'].update({'username': username, 'password': password},
                                                                       {'$set': {'email_bak': email_bak}})
                return HttpResponse(json.dumps({'msg': 1}))


def identification_photo(request):
    """
    上传证件照
    :param request:
    :return:
    """
    username = request.COOKIES.get('udfuenf')
    password = request.COOKIES.get('pdfuenf')
    if 'audit_a' not in request.FILES:
        return HttpResponse(json.dumps({'msg': '请上传证件照正面！'}))
    if 'audit_b' not in request.FILES:
        return HttpResponse(json.dumps({'msg': '请上传证件照反面！'}))
    a_path = 'http://106.75.91.135/wotouchain/identification_photo/{}-a'.format(username) + '.png'
    a_save_path = '/data/wotouchain/identification_photo/{}-a'.format(username) + '.png'
    b_path = 'http://106.75.91.135/wotouchain/identification_photo/{}-b'.format(username) + '.png'
    b_save_path = '/data/wotouchain/identification_photo/{}-b'.format(username) + '.png'
    audit_a_img = request.FILES.get('audit_a')
    audit_b_img = request.FILES.get('audit_b')
    #
    with open(a_save_path, 'wb') as f:
        for chunk in audit_a_img.chunks(chunk_size=1024):
            f.write(chunk)
    with open(b_save_path, 'wb') as f:
        for chunk in audit_b_img.chunks(chunk_size=1024):
            f.write(chunk)
    mongodb_connection('server')['wotouchain_user'].update({'username': username, 'password': password},
                                                           {'$set': {'audit': '审核中', 'audit_a_img': a_path,
                                                                     'audit_b_img': b_path}})
    return HttpResponse(json.dumps({'msg': 1}))


def project_detail(request):
    """
    项目详情页面
    :param request:
    :return:
    """
    return render(request, 'project_detail.html')


def project_audit(request):
    """
    管理员控制台动态加载的项目审核页面
    :param request:
    :return:
    """
    if request.method == 'POST':
        pid=request.POST.get('pid')
        p_detail = mongodb_connection('server')['wotouchain_project'].find_one({'project_index':pid})
        return render(request, 'project-audit.html',p_detail)
